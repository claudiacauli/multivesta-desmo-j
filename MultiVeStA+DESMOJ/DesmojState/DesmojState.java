package vesta.desmoj;

import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.ModelCondition;
import desmoj.core.simulator.TimeInstant;
import desmoj.core.simulator.TimeOperations;
import desmoj.core.simulator.TimeSpan;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import vesta.mc.IStateEvaluator;
import vesta.mc.NewState;
import vesta.mc.ParametersForState;

public class DesmojState
  extends NewState
{
  private static final long serialVersionUID = 3473827487239473248L;
  private Experiment desmoExperiment;
  private Model desmoModel;
  private String myStopConditionClassName;
  static ModelCondition stopCondition;
  private String modelAndExperimentParameters;
  
  public DesmojState(ParametersForState paramParametersForState)
  {
    super(paramParametersForState);
    this.myStopConditionClassName = null;
    
    this.modelAndExperimentParameters = paramParametersForState.getModel();
  }
  
  public Model getModel()
  {
    return this.desmoModel;
  }
  
  public Experiment getExperiment()
  {
    return this.desmoExperiment;
  }
  
  public double getTime()
  {
    return this.desmoExperiment.getModel().presentTime().getTimeAsDouble();
  }
  
  public void performOneStepOfSimulation()
  {
    if (!this.desmoExperiment.isRunning())
    {
      this.desmoExperiment.stop(new TimeInstant(this.desmoExperiment.getModel().presentTime().getTimeAsDouble() + 1.0D));
      this.desmoExperiment.start();
    }
    else
    {
      this.desmoExperiment.stop(new TimeInstant(this.desmoExperiment.getModel().presentTime().getTimeAsDouble() + 1.0D));
      this.desmoExperiment.proceed();
    }
  }
  
  public void performWholeSimulation()
  {
    this.desmoExperiment.start();
  }
  
  public void setSimulatorForNewSimulation(int paramInt)
  {
    this.desmoExperiment = new Experiment("Experiment");
    this.desmoExperiment.setSeedGenerator(paramInt);
    readXMLconfigurationFile();
  }
  
  public double rval(int paramInt)
  {
    switch (paramInt)
    {
    case 0: 
      if ((this.myStopConditionClassName != null) && (stopCondition.check() == true) && 
        (getTime() > 0.0D))
      {
        this.desmoExperiment.report();
        this.desmoExperiment.finish();
        return 1.0D;
      }
      return 0.0D;
    case 1: 
      getTime();
    case 2: 
      getNumberOfSteps();
    case 3: 
      if (this.desmoExperiment.isRunning()) {
        return 1.0D;
      }
      return 0.0D;
    case 4: 
      if (this.desmoExperiment.isStopped()) {
        return 1.0D;
      }
      return 0.0D;
    case 5: 
      if (this.desmoExperiment.isConnected()) {
        return 1.0D;
      }
      return 0.0D;
    case 6: 
      return TimeOperations.getEpsilonSpan().getTimeAsDouble();
    }
    return getStateEvaluator().getVal(paramInt, this);
  }
  
  public double rval(String paramString)
  {
    return 0.0D;
  }
  
  private void readXMLconfigurationFile()
  {
    File localFile = new File(this.modelAndExperimentParameters);
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    try
    {
      DocumentBuilder localDocumentBuilder = localDocumentBuilderFactory.newDocumentBuilder();
      Document localDocument = localDocumentBuilder.parse(localFile);
      localDocument.getDocumentElement().normalize();
      NodeList localNodeList1 = localDocument.getElementsByTagName("parameters");
      if ((localNodeList1 != null) && (localNodeList1.getLength() > 0)) {
        for (int i = 0; i < localNodeList1.getLength(); i++)
        {
          Element localElement = (Element)localNodeList1.item(i);
          NodeList localNodeList2 = localElement.getChildNodes();
          if ((localNodeList2 != null) && (localNodeList2.getLength() > 0)) {
            for (int j = 0; j < localNodeList2.getLength(); j++)
            {
              Node localNode = localNodeList2.item(j);
              String str1 = localNode.getNodeName();
              String str2 = localNode.getTextContent();
              switch (str1)
              {
              case "modelClassPath": 
                try
                {
                  this.desmoModel = ((Model)Class.forName(str2).newInstance());
                }
                catch (InstantiationException|IllegalAccessException|ClassNotFoundException localInstantiationException1)
                {
                  localInstantiationException1.printStackTrace();
                }
                this.desmoModel.connectToExperiment(this.desmoExperiment);
                break;
              case "debugOn": 
                if (str2.equals("true")) {
                  this.desmoModel.debugOn();
                } else {
                  this.desmoModel.debugOff();
                }
                break;
              case "debugOff": 
                if (str2.equals("true")) {
                  this.desmoModel.debugOff();
                } else {
                  this.desmoModel.debugOn();
                }
                break;
              case "reportOn": 
                if (str2.equals("true")) {
                  this.desmoModel.reportOn();
                } else {
                  this.desmoModel.reportOff();
                }
                break;
              case "reportOff": 
                if (str2.equals("true")) {
                  this.desmoModel.reportOff();
                } else {
                  this.desmoModel.reportOn();
                }
                break;
              case "traceOn": 
                if (str2.equals("true")) {
                  this.desmoModel.traceOn();
                } else {
                  this.desmoModel.traceOff();
                }
                break;
              case "traceOff": 
                if (str2.equals("true")) {
                  this.desmoModel.traceOff();
                } else {
                  this.desmoModel.traceOn();
                }
                break;
              case "debugOffAt": 
                this.desmoExperiment.debugOff(new TimeInstant(Long.parseLong(str2))); break;
              case "debugOnAt": 
                this.desmoExperiment.debugOn(new TimeInstant(Long.parseLong(str2))); break;
              case "delayInMillis": 
                this.desmoExperiment.setDelayInMillis(Long.parseLong(str2)); break;
              case "executionSpeedRate": 
                this.desmoExperiment.setExecutionSpeedRate(Long.parseLong(str2)); break;
              case "traceOnAt": 
                this.desmoExperiment.traceOn(new TimeInstant(Long.parseLong(str2))); break;
              case "traceOffAt": 
                this.desmoExperiment.traceOff(new TimeInstant(Long.parseLong(str2))); break;
              case "stopAt": 
                this.desmoExperiment.stop(new TimeInstant(Long.parseLong(str2))); break;
              case "stopConditionClassPath": 
                if (str2 != null) {
                  try
                  {
                    Constructor localConstructor = null;
                    try
                    {
                      Class localClass = Class.forName(str2);
                      localConstructor = localClass.getConstructor(new Class[] { Model.class, String.class, Boolean.TYPE });
                    }
                    catch (NoSuchMethodException|SecurityException localNoSuchMethodException)
                    {
                      localNoSuchMethodException.printStackTrace();
                    }
                    try
                    {
                      stopCondition = (ModelCondition)localConstructor.newInstance(new Object[] { this.desmoModel, "", Boolean.valueOf(false) });
                    }
                    catch (IllegalArgumentException|InvocationTargetException localIllegalArgumentException)
                    {
                      localIllegalArgumentException.printStackTrace();
                    }
                  }
                  catch (InstantiationException|IllegalAccessException|ClassNotFoundException localInstantiationException2)
                  {
                    localInstantiationException2.printStackTrace();
                  }
                }
                this.desmoExperiment.stop(stopCondition);
                this.myStopConditionClassName = str2;
                break;
              case "showProgressBar": 
                if (str2.equals("true")) {
                  this.desmoExperiment.setShowProgressBar(true);
                } else {
                  this.desmoExperiment.setShowProgressBar(false);
                }
                break;
              case "setSilent": 
                if (str2.equals("true")) {
                  this.desmoExperiment.setSilent(true);
                } else {
                  this.desmoExperiment.setSilent(false);
                }
                break;
              case "#text": 
                break;
              case "#comment": 
                break;
              default: 
                System.out.println("The " + str2 + " parameter is not currently supported.");
              }
            }
          }
        }
      }
    }
    catch (ParserConfigurationException|SAXException|IOException localParserConfigurationException)
    {
      localParserConfigurationException.printStackTrace();
    }
  }
}
