package mv;

import game.SaMurraModel;
import vesta.desmoj.DesmojState;
import vesta.mc.IStateEvaluator;
import vesta.mc.NewState;

/**
 * Class which implements the model-specific observations required by
 * MultiVeStA while running the experiments. 
 * Seeing the Model's SubClass of the current experiment, which in this
 * case is SaMurraModel, it is allowed to access its methods.
 * 
 * @author Claudia Cauli
 *
 */
public class SaMurraEvaluator implements IStateEvaluator 
{
	/**
	 * Constructs an object of this class invoking the super constructor.
	 */
	public SaMurraEvaluator(){
		super();
	}
	
	/**
	 * This Method runs the model-specific observations.
	 * 
	 * @param paramInt
	 * 		An integer to be used to, eventually, switch among different cases.
	 * @param genericState
	 * 		The current State invoking this method.
	 * @return
	 * 		A double which will be used in QuaTex expressions.
	 */
	public double getVal(int paramInt, NewState genericState) {
		DesmojState desmoState = (DesmojState) genericState;
		SaMurraModel model = (SaMurraModel) desmoState.getModel();
		
		if (model.getWinner().equals("firstPlayer")) 
			return 1.0D;
		else 
			return 0.0D;
	}
	
	// Required??
	@Override
	public double getVal(String arg0, NewState arg1) {
		return 0;
	}
	
}