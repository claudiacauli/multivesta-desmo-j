package game;
import java.util.Random;

import desmoj.core.simulator.Model;
import desmoj.core.simulator.SimProcess;
import desmoj.core.simulator.TimeSpan;

/**
 * This class represents a Player, implemented as a Simulated Process, who is
 * characterized by his/her score, current choice and
 * ability. 
 * Players' role is centered on their lifeCycle() method.  
 * 
 * @author Claudia Cauli
 *
 */
public class Player extends SimProcess 
{
	/**
	 * A Random object to generate random numbers.
	 */
	private Random randomGenerator;
	
	/**
	 * The current score of this Player
	 */
	private int score;
	
	/**
	 * The ability degree of this Player. Ad integer from 1 to 5.
	 * 1 Extremely dumb,
	 * 2 Amateur player,
	 * 3 Intermediate Player,
	 * 4 Sa Murra Lover,
	 * 5 Proficient in Sa Murra. 
	 */
	private int ability;
	
	/**
	 * The last choice made by this Player.
	 */
	private int choice;
	
	/**
	 * Constructs a Player, setting her/his ability.
	 * @param owner
	 * 			The model to which this player belongs to.
	 * @param name
	 * 			The name of this Player.
	 * @param showInTrace
	 * 			A flag indicating if this Player will be shown in the Traces.
	 * @param ability
	 * 			Player's ability.
	 */
	public Player (Model owner, String name, boolean showInTrace, int ability) {
		super(owner, name, showInTrace);
		this.randomGenerator = new Random();
		this.score = 0;
		this.ability = ability;
	}
	
	/**
	 * Method which returns the score of this player.
	 * @return 
	 * 		Current score of this Player.
	 */
	public int getCurrentPoints(){
		return this.score;
	}
	
	/**
	 * Increases the score by one.
	 */
	public void increaseCurrentPoints(){
		this.score++;
	}
	
	/**
	 * Guesses the sum of the numbers chosen by this Player and his/her
	 * opponent. The algorithm allows for players' ability in defining the most
	 * narrow range of possible numbers.  
	 * This range is a set which cardinality goes from 5 (Proficient players),
	 * the uniquely possible numbers, to 9 (Dumb players), four of which surely 
	 * won't be the exact results of the sum.
	 *  
	 * @return The guessed integer.
	 * 		
	 */
	public int hazardsAguess(){
		if (this.choice < this.ability)
			return this.choice+1+randomGenerator.nextInt(10-this.ability);
		else 
			return this.ability+1+randomGenerator.nextInt(10-this.ability);
	}
	
	/**
	 * Chooses a number to use in this play, as a random integer from 1 to 5.
	 * 
	 * @return The chosen integer.
	 */
	public int showsTheirFingers(){
		this.choice = 1 + randomGenerator.nextInt(5);
		return this.choice;
	}
	
	/**
	 * The core method of a Player's role. 
	 * The player stores, in the variable "myMatch", the Match which activated its process. 
	 * In turn, the player's process re-activates it, once finished its tasks.
	 * In its lifeCycle a Player's process holds for a while, before choosing a number, and 
	 * for another while, before guessing another.  
	 * Finally, it passivates, waiting for some other processes to re-activate it.
	 */
	public void lifeCycle(){
		while (true){
			Match myMatch = (Match)getActivatedBy();
			
			this.hold(new TimeSpan (SaMurraModel.CHOICE_DELAY.sample()));
			myMatch.setChoice(this.showsTheirFingers(),this.getIdentNumber());
			
			this.hold(new TimeSpan (SaMurraModel.GUESS_DELAY.sample()));;
			myMatch.setGuess(this.hazardsAguess(),this.getIdentNumber());
			
			myMatch.activate();
			this.passivate();
		}
	}
	
	
}