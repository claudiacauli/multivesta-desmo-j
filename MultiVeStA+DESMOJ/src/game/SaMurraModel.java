package game;
import desmoj.core.dist.ContDistNormal;
import desmoj.core.dist.ContDistUniform;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;

/**
 * This class extends Model and refers to all its associated 
 * components. In order to easily set the experiment's parameters, 
 * it declares and initializes: the points-per-match, three different 
 * delays (as Distributions), the match itself, and a ModelCondition's
 * subclass "matchIsOver" (which provides the experiment with the stop-condition).  
 * 
 * NOTICE/MULTIVESTA: 
 * This class will be passed as an argument while running MultiVeStA.
 * The main method is missing due to MultiVeStA's role in
 * initializing, connecting and running the experiment.
 * 
 * @author Claudia Cauli
 * 
 */
public class SaMurraModel extends Model {
	
	/**
	 * Points to reach in order to win a match.
	 */
	static int MAX_POINTS_PER_MATCH;
	
	/**
	 * Normal Distribution which samples will introduce delays in players' choices 
	 */
	static ContDistNormal CHOICE_DELAY;
	
	/**
	 * Uniform Distribution which samples will introduce delays in players' guesses.
	 */
	static ContDistUniform GUESS_DELAY;
	
	/**
	 * Uniform Distribution which samples will introduce delays in the score's evaluation. 
	 */
	static ContDistUniform CHECK_DELAY;
	
	/**
	 * ModelCondition checking the stop-clause.
	 */
	static MatchIsOver stopCondition;
	
	/**
	 * Boolean to be handled by the aforementioned ModelCondition 
	 */
	private boolean matchIsOver;
	
	/**
	 * TimeSpan duration of the match
	 */
	private TimeSpan duration;
	
	/**
	 * String containing the winner's name.
	 */
	private String winner;
	
	
	/**
	 * Constructs this Model, whit parameters:
	 * @param owner
	 * 			The model to which this belongs to.
	 * @param name
	 * 			This model's name
	 * @param showInReport
	 * 			Flag for showing this model in report-files
	 * @param showInTrace
	 * 			Flag for showing this model in trace-files
	 */
	public SaMurraModel (Model owner, 
						 String name, 
						 boolean showInReport, 
						 boolean showInTrace) {
		super (owner, name, showInReport, showInTrace);
	}
	
	/**
	 * Empty Constructor of this model.
	 * NOTICE/MULTIVESTA: This empty constructor helps MultiVeStA to easily 
	 * build the model class in its initial schedules. 
	 */
	public SaMurraModel(){
		super(null, "Model", true, true);
	}
	
	/**
	 * Method which initializes and instantiates all the model's components. 
	 * It prevents them to interfere with an experiment until its beginning. 
	 */
	public void init(){
		// Initializing the max points per match.
		SaMurraModel.MAX_POINTS_PER_MATCH = 16;
		
		// Initializing delays.
		SaMurraModel.CHOICE_DELAY = new ContDistNormal(this, "Choice-Delay", 1.0, 0.2, true, true) ;
		SaMurraModel.GUESS_DELAY  = new ContDistUniform(this, "Guess-Delay", 0.01, 0.05, true, true);
		SaMurraModel.CHECK_DELAY  = new ContDistUniform(this, "Check-Delay", 0.05, 0.1, true, true);
		
		// Initializing other variables.
		this.matchIsOver = false;
		this.duration= null;
		this.winner = null;
	}

	/**
	 * Method which schedules the MatchGenerator SimProcess.
	 */
	public void doInitialSchedules(){
		// Creates the two players.
		Player firstPlayer =  new Player(this, "First Player", true, 5);
		Player secondPlayer = new Player(this, "Second Player", true, 3);
	
		// Creates and activates the match.
		Match match = new Match (this, "Match", true, firstPlayer, secondPlayer);
		match.activate();
	}

	/**
	 * Description of this Model
	 */
	public String description(){
		return "This Model simulates a match of the popular Sardinian game known as "
				+ "\"Sa Murra\". "
				+ "In each play of a match, two players choose a number and guess a sum. "
				+ "The player whose guess is right wins a point. The first to reach "
				+ "a score of "+SaMurraModel.MAX_POINTS_PER_MATCH+" wins the match. \n"
				+ "Each player has an ability degree, an integer from 1 to 5, where "
				+ "1 stands for \"Extremely dumb\" and 5 stands for \"Proficient in "
				+ "Sa Murra\".";
	}
	
	/**
	 * Getter method of the winner.
	 * @return A String containing the name of the winner
	 */
	public String getWinner(){
		return this.winner;
	}
	
	/**
	 * Method to set the winner, used by the match's object.
	 * @param who
	 * 			Contains the String to set.
	 */
	public void setWinner(String who){
		this.winner = who;
	}
	
	/**
	 * Setter method of the match's duration
	 * @param duration
	 * 			The TimeSpan duration of the performed match.
	 */
	public void setDuration(TimeSpan duration){
		this.duration = duration;
	}
	
	/**
	 * Getter method of the match's duration
	 * @return The TimeSpan duration of the performed match.
	 */
	public TimeSpan getDuration(){
		return this.duration;
	}
	
	/**
	 * Method getting whether the match is over or not.
	 * @return The boolean field matchIsOver
	 */
	public boolean getMatchIsOver(){
		return this.matchIsOver;
	}
	
	/**
	 * Setter method of the boolean matchIsOver
	 * @param isOver
	 * 			A boolean to overwrite matchIsOver field.
	 */
	public void setMatchIsOver(boolean isOver){
		this.matchIsOver=isOver;
	}

}