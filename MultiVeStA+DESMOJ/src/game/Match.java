package game;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.SimProcess;
import desmoj.core.simulator.TimeInstant;
import desmoj.core.simulator.TimeOperations;
import desmoj.core.simulator.TimeSpan;

/**
 * This class represents a single Match and is implemented as a subclass
 * of SimProcess. A match is characterized by its two Players, its final
 * winner, and their current choices and guesses.
 * 
 * @author Claudia Cauli
 *
 */
public class Match extends SimProcess {

	/**
	 * Model to which this Match belongs to.
	 */
	private SaMurraModel model;
	
	/**
	 * The match's winner.
	 */
	private Player winner;
	
	/**
	 * The first Player.
	 */
	private Player firstPlayer;
	
	/**
	 * The second Player.
	 */
	private Player secondPlayer;
	
	/**
	 * Integer which stores the current first Player's choice.
	 */
	private int firstPlayersChoice;
	
	/**
	 * Integer which stores the current second Player's choice.
	 */
	private int secondPlayersChoice;
	
	/**
	 * Integer which stores the current first Player's guess.
	 */
	private int firstPlayersGuess;
	
	/**
	 * Integer which stores the current second Player's guess.
	 */
	private int secondPlayersGuess;
	
	/**
	 * TimeInstant containing the start time of the match.
	 */
	private TimeInstant startTime;
	
	
	/**
	 * Constructs a Match. With the given two Players.
	 * 
	 * @param owner
	 * 			The Model to which this Match belongs to.
	 * @param name
	 * 			The name of this Match.
	 * @param showInTrace
	 * 			A flag indicating if this match will be shown in traces.
	 * @param firstPlayer
	 * 			The first Player.
	 * @param secondPlayer
	 * 			The second Player.
	 */
	public Match (Model owner, String name, boolean showInTrace, Player firstPlayer, Player secondPlayer) {
		super (owner, name, showInTrace);
		
		this.model = (SaMurraModel)owner;
		this.winner= null;
		this.firstPlayer  = firstPlayer;
		this.secondPlayer = secondPlayer;
	}
	
	/**
	 * Method used by the two Players in their lifeCycle() to tell the Match their
	 * choices.
	 * @param choice
	 * 			The number chosen by the Player
	 * @param id
	 * 			The ID of the Player.
	 */
	public void setChoice (int choice, long id) {
		if (id == firstPlayer.getIdentNumber()){
			this.firstPlayersChoice = choice;
		}
		else {
			this.secondPlayersChoice = choice;
		}
	}
	
	/**
	 * Method used by the two Players to tell the Match their guesses. 
	 * @param guess
	 * 			The number guessed by the Player.
	 * @param id
	 * 			Player's ID.
	 */
	public void setGuess (int guess, long id) {
		if (id == firstPlayer.getIdentNumber()){
			this.firstPlayersGuess = guess;
		}
		else {
			this.secondPlayersGuess = guess;
		}
	}

	/**
	 * Method to check which player, if any or both, wins the current play.
	 */
	public void checkPoints (){
		if (firstPlayersGuess == firstPlayersChoice+secondPlayersChoice){
			this.firstPlayer.increaseCurrentPoints();
		}
		if (secondPlayersGuess == firstPlayersChoice+secondPlayersChoice){
			this.secondPlayer.increaseCurrentPoints();
		}
	}

	/**
	 * Checks if a player, none or both, wins the whole Match. 
	 * If both players reach a score of 16 the match goes on until
	 * one surpasses the other.
	 */
	public void checkWinner () {
		if (this.firstPlayer.getCurrentPoints() == SaMurraModel.MAX_POINTS_PER_MATCH && 
			this.secondPlayer.getCurrentPoints() == SaMurraModel.MAX_POINTS_PER_MATCH){}
		else if (this.firstPlayer.getCurrentPoints() == SaMurraModel.MAX_POINTS_PER_MATCH ||
				(this.firstPlayer.getCurrentPoints() > this.secondPlayer.getCurrentPoints() &&
				 this.firstPlayer.getCurrentPoints() > SaMurraModel.MAX_POINTS_PER_MATCH)){
			this.winner = this.firstPlayer;
			this.model.setWinner("firstPlayer");
			this.model.setMatchIsOver(true);;
			System.out.println ("The FIRST Player Wins!!!");
		}
		else if (this.secondPlayer.getCurrentPoints() == SaMurraModel.MAX_POINTS_PER_MATCH ||
				(this.secondPlayer.getCurrentPoints() > this.firstPlayer.getCurrentPoints() && 
				 this.firstPlayer.getCurrentPoints()> SaMurraModel.MAX_POINTS_PER_MATCH)){
			this.winner = this.secondPlayer;
			this.model.setWinner("secondPlayer");
			this.model.setMatchIsOver(true);;
			System.out.println ("The SECOND Player Wins!!!");
		}
		
	}
	
	/**
	 * The lifeCycle() method of a Match. 
	 * Until none of the Players' wins, the match launches different plays in succession.
	 * In each play, the Match SimProcess activates both Players and passivates twice, waiting
	 * for both to re-activate its execution, once completed their tasks.
	 * Then, the match holds for a tiny TimeSpan before keeping the score and checking 
	 * whether there's a winner or not.	 
	 */
	public void lifeCycle(){
			
		this.startTime = this.presentTime();
		
		while (this.winner==null){
			firstPlayer.activate();
			secondPlayer.activate();
			this.passivate();
			this.passivate();
			hold (new TimeSpan (SaMurraModel.CHECK_DELAY.sample()));
			this.checkPoints();
			this.checkWinner();
		}
		
		this.model.setDuration(TimeOperations.diff(this.presentTime(), this.startTime));
		
	}
		
}