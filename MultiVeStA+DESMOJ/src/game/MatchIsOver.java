package game;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.ModelCondition;

/**
 * This class extends ModelCondition. It has a method that checks 
 * the fulfillment of a certain state, returning a true value to use as the
 * stop clause of the whole experiment. In this case, the achievement of the
 * fixed number of points in a match, leading one player to the victory.
 * 
 * NOTICE/MULTIVESTA: 
 * This class is passed as an argument while running MultiVeStA.
 * It is instantiated in DesmojState class and used to check whether
 * the experiment has finished.
 * 
 * @author Claudia Cauli
 *
 */
public class MatchIsOver extends ModelCondition {

	/**
	 * Model to which this class belongs to.
	 */
	private SaMurraModel model;
	
	/**
	 * Constructs this subclass of ModelCondition. 
	 * @param owner 
	 * 			The model to which this class belongs to.
	 * @param name
	 * 			The name of this class.
	 * @param showInTrace
	 * 			A flag indicating whether to show this in the final Traces or not.
	 */
	public MatchIsOver (Model owner, String name, boolean showInTrace){
		super (owner, name, showInTrace);
		this.model=(SaMurraModel)owner;
	}
	
	/**
	 * Core method of this ModelCondition. It checks if one player has won.
	 */
	public boolean check(){
		return model.getMatchIsOver();
	}
	
}